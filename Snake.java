public class Snake{
	public String breed;
	public double length;
	public boolean venomous;
	
	public Snake(String breed, double length, boolean venomous){
		this.breed = breed;
		this.length = length;
		this.venomous = venomous;
	}
	
	public void beCautious() {
		if (venomous == true){
			System.out.print("Be cautious! The " + breed + " is venomous");
		} else {
			System.out.print("No need to worry " + breed + " is not venomous");
		}
	}

	public void snakeSize() {
		if (length >= 6.25){
			System.out.println(" and it is huge!");
		} else {
			System.out.println(" and it is quite small.");
		}
	}
}