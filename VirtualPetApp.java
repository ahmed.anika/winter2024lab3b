import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {
        String[] animals = new String[]{"a pod of Dolphins", "a swarm of Bees", "a herd of Deer", "a troop of Kangaroos"};
        Scanner reader = new Scanner(System.in);

		Snake dolphins = null;
        Snake bees = null;
        Snake deer = null;
        Snake kangaroos = null;

        for (int i = 0; i < animals.length; i++) {
			
            System.out.println("What breed is it?");
            String breed = reader.nextLine();
			
	
            System.out.println("How big is it? (in meters)");
            double length = reader.nextDouble();
			

            System.out.println("Is it venomous?(true or false)");
            boolean venomous = reader.nextBoolean();
			reader.nextLine();
			
			if (i == 0){
				 dolphins = new Snake(breed, length, venomous);
			} else if (i == 1){
				 bees = new Snake(breed, length, venomous);
			} else if (i == 2){
				 deer = new Snake(breed, length, venomous);
			} else if (i == 3){
				 kangaroos = new Snake(breed, length, venomous);
			}
        }

		System.out.println(kangaroos.breed);
		System.out.println(kangaroos.length);
		System.out.println(kangaroos.venomous);

		dolphins.beCautious();
		dolphins.snakeSize();
    }
}
